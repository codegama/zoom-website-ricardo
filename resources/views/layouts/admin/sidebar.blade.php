<aside class="left-sidebar sidebar-dark" id="left-sidebar">

    <div id="sidebar" class="sidebar sidebar-with-footer">
        <!-- Aplication Brand -->
        <div class="app-brand">
            <a href="{{route('admin.dashboard')}}">
                <!-- <img src="{{Setting::get('site_logo')}}" alt="{{Setting::get('site_name')}}" class="logo"> -->
                <span class="brand-name text-uppercase">{{Setting::get('site_name')}}</span>
            </a>
        </div>
        <!-- begin sidebar scrollbar -->
        <div class="sidebar-left" data-simplebar style="height: 100%;">
            <!-- sidebar menu -->
            <ul class="nav sidebar-inner" id="sidebar-menu">

                <li class="has-sub" id="dashboard">
                    <a class="sidenav-item-link" href="{{route('admin.dashboard')}}">
                        <i class="mdi mdi-briefcase-account-outline"></i>
                        <span class="nav-text">{{tr('dashboard')}}</span>
                    </a>
                </li>

                <li class="section-title">
                    {{tr('account_management')}}
                </li>

                <li class="has-sub" id="users_crud">

                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#users" aria-expanded="false" aria-controls="users">
                        <i class="mdi mdi-account-multiple"></i>
                        <span class="nav-text">{{tr('users')}}</span> <b class="caret"></b>
                    </a>
                    <ul class="collapse" id="users" data-parent="#sidebar-menu">
                        <div class="sub-menu">

                            <li id="users-create">
                                <a class="sidenav-item-link" href="{{route('admin.users.create')}}">
                                    <span class="nav-text">{{tr('add_user')}}</span>

                                </a>
                            </li>

                            <li id="users-index">
                                <a class="sidenav-item-link" href="{{route('admin.users.index')}}">
                                    <span class="nav-text">{{tr('view_users')}}</span>

                                </a>
                            </li>

                        </div>
                    </ul>
                </li>

                <li class="section-title">
                    {{tr('meetings_management')}}
                </li>

                <li class="has-sub" id="meetings">

                    <a class="sidenav-item-link" href="{{route('admin.meetings.index')}}">
                        <i class="mdi mdi-ticket"></i>
                        <span class="nav-text">{{tr('meetings')}}</span>
                    </a>
                </li>

                <li class="section-title">
                    {{tr('payment_management')}}
                </li>

                <li class="has-sub" id="subscriptions_crud">

                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#subscriptions" aria-expanded="false" aria-controls="subscriptions">
                        <i class="mdi mdi-diamond"></i>
                        <span class="nav-text">{{tr('subscriptions')}}</span> <b class="caret"></b>
                    </a>
                    <ul class="collapse" id="subscriptions" data-parent="#sidebar-menu">
                        <div class="sub-menu">

                            <li id="subscriptions-create">
                                <a class="sidenav-item-link" href="{{route('admin.subscriptions.create')}}">
                                    <span class="nav-text">{{tr('add_subscription')}}</span>

                                </a>
                            </li>

                            <li id="subscriptions-view">
                                <a class="sidenav-item-link" href="{{route('admin.subscriptions.index')}}">
                                    <span class="nav-text">{{tr('view_subscriptions')}}</span>

                                </a>
                            </li>

                        </div>
                    </ul>
                </li>

                <li class="has-sub" id="subscription_payments">

                    <a class="sidenav-item-link" href="{{route('admin.subscription.payments')}}">
                        <i class="mdi mdi-ticket"></i>
                        <span class="nav-text">{{tr('subscription_payments')}}</span>
                    </a>
                    
                </li>

                <li class="section-title">
                    {{tr('setting_management')}}
                </li>
                <li class="has-sub" id="static_pages_crud">
                    <a class="sidenav-item-link" href="javascript:void(0)" data-toggle="collapse" data-target="#static_pages" aria-expanded="true" aria-controls="static_pages">
                        <i class="mdi mdi-file-multiple"></i>
                        <span class="nav-text">{{tr('static_pages')}}</span> <b class="caret"></b>
                    </a>
                    <ul class="collapse" id="static_pages" data-parent="#sidebar-menu">
                        <div class="sub-menu">

                            <li id="static_pages_create">
                                <a class="sidenav-item-link" href="{{route('admin.static_pages.create')}}">
                                    <span class="nav-text">{{tr('add_static_page')}}</span>

                                </a>
                            </li>

                            <li id="static_pages_view">
                                <a class="sidenav-item-link" href="{{route('admin.static_pages.index')}}">
                                    <span class="nav-text">{{tr('view_static_pages')}}</span>

                                </a>
                            </li>

                        </div>
                    </ul>
                </li>

                <li id="settings">
                    <a class="sidenav-item-link" href="{{route('admin.settings')}}">
                        <i class="mdi mdi-settings"></i>
                        <span class="nav-text">{{tr('settings')}}</span>
                    </a>
                </li>

                <li class="section-title">
                    {{tr('profile_management')}}
                </li>

                <li id="profile">
                    <a class="sidenav-item-link" href="{{route('admin.profile')}}">
                        <i class="mdi mdi-account"></i>
                        <span class="nav-text">{{tr('profile')}}</span>
                    </a>
                </li>

                <li>
                    <a class="sidenav-item-link" href="{{route('admin.logout')}}"   data-toggle="modal" data-target="#logoutModel">
                        <i class="mdi mdi-logout"></i>
                        <span class="nav-text">{{tr('logout')}}</span>
                    </a>
                </li>

            </ul>

        </div>

        
    </div>
</aside>


<script type="text/javascript">

	// Target id == Page 
	@if(isset($main_page)) 
        var id = "{{$main_page}}";
        $("#"+"{{$main_page}}").addClass("active expand");
        $("#"+"{{$main_page}}").find('.sidenav-item-link').click(); 
    @endif

    @if(isset($page))
        $("#{{$page}}").addClass("show");
    @endif
    
    @if(isset($sub_page)) 
        $("#{{$sub_page}}").addClass("active");
    @endif

</script>