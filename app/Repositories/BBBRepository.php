<?php

namespace App\Repositories;

use App\Helpers\Helper;

use Log, Validator, Setting, Exception, DB;

use BigBlueButton\BigBlueButton;

use BigBlueButton\Parameters\CreateMeetingParameters;

use SimpleXMLElement;

class BBBRepository {

    /**
     * @method bbb_meeting_create()
     *
     * @uses 
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $subscription_details, object $request
     *
     * @return object $subscription_details
     */

    public static function bbb_meeting_create($request, $meeting) {

        try {

            $params['meetingID'] = $meeting->unique_id; 

            $params['recordID'] = "record-".$meeting->unique_id; 

            $params['meetingName'] = $meeting->meeting_name;

            $params['name'] = $meeting->meeting_name;

            $params['moderatorPW'] = 'mp'.$meeting->id;

            $params['attendeePW'] = 'ap'.$meeting->id;

            $logout_url = route('meetings_users_logout', ['meeting_id' => $meeting->unique_id]);

            $params['logoutURL'] = $logout_url;

            $end_call_back_url = route('meetings_end', ['meeting_id' => $meeting->unique_id]);

            $params['endCallbackUrl'] = $end_call_back_url;

            $params['logo'] = Setting::get('site_logo');

            $share_link = Setting::get('frontend_url').'join/'.$meeting->unique_id;

            $params['welcome'] = "<br>Welcome to <b>%%CONFNAME%%</b>! <br><br>Share Link: <a href=".$share_link.">".$share_link."</a>";

            $params['copyright'] = Setting::get('site_name');

            $params['record'] = Setting::get('meeting_recording') == YES ? 'true' : 'false';

            $params['allowStartStopRecording'] = Setting::get('meeting_recording') == YES ? 'true' : 'false';

            $params['autoStartRecording'] = 'false';

            $no_of_minutes = $meeting->no_of_minutes ? intval($meeting->no_of_minutes) : 60;

            $params['duration'] = $no_of_minutes;

            $no_of_users = $meeting->no_of_users ?: 4;

            $params['maxParticipants'] = $no_of_users+1;

            $params['meta_endCallbackUrl'] = $end_call_back_url;

            $params['meta_bbb-recording-ready-url'] = route('meeting_records_save', ['meeting_unique_id' => $meeting->unique_id]);

            // $params['meta_endCallbackUrl'] = http_build_query(route('meetings_end', ['meeting_unique_id' => $meeting->unique_id]), null, '&');

            // $params['meta_bbb-recording-ready-url'] = http_build_query(route('meeting_records_save', ['meeting_unique_id' => $meeting->unique_id]), null, '&');

            $bbb_curl_url = self::generate_salt($params);

            $response = self::bbb_meeting_curl_execute($bbb_curl_url);

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }
    
    }

    /**
     * @method bbb_meeting_join()
     *
     * @uses 
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $subscription_details, object $request
     *
     * @return object $subscription_details
     */

    public static function bbb_meeting_join($request, $meeting, $meeting_member = []) {

        try {
            
            $params['meetingID'] = $meeting->unique_id; 

            $params['fullName'] = $request->username;

            $params['redirect'] = 'true';

            $logout_url = route('meetings_users_logout', ['meeting_id' => $meeting->meeting_unique_id, 'meeting_member_id' => $meeting_member->id ?? 0]);

            $params['logoutURL'] = $logout_url;

            if($request->moderatorPW) {
                $params['password'] = $request->moderatorPW;
            }

            if($request->attendeePW) {
                $params['password'] = $request->attendeePW;
            }

            $bbb_curl_url = self::generate_salt($params, $method = "join");

            $response = ['success' => true, 'data' => $bbb_curl_url];

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }
    
    }

    public static function generate_salt($params_array, $method = 'create') {

        $BBB_SERVER_BASE_URL = get_bbb_base_url();

        $BBB_SECRET = get_bbb_secret();

        $params = http_build_query(array_filter($params_array));

        $append = 1;

        $parameters = $append ? '?' . $params.'&checksum=' . sha1($method . $params . $BBB_SECRET) : "";

        $base_url = $BBB_SERVER_BASE_URL . 'api/'. $method;

        return $base_url. $parameters;
    
    }

    public static function bbb_meeting_curl_execute($bbb_curl_url) {

        try {

            \Log::info($bbb_curl_url);

            if(!extension_loaded('curl')) {

                throw new Exception("Post XML data set but curl PHP module is not installed or not enabled.", 101);
            }

            // $pdf_xml = self::add_presentation('http://bbschool-backend.codegama.net/Welcome-to-BBB-School.pdf');

            // $curl = new \anlutro\cURL\cURL;

            /**$curl_response = $curl->rawPost($bbb_curl_url, '<?xml version="1.0" encoding="UTF-8"?><modules><module name="presentation"><document url="http://bbschool-backend.codegama.net/Welcome-to-BBB-School.pdf" filename="report.pdf"/><document name="sample-presentation.pdf">JVBERi0xLjQKJ></document></module></modules>'); */

            // Send a GET request to: http://www.foo.com/bar
            $curl_response = \Curl::to($bbb_curl_url)->get();

            if($curl_response == "" || $curl_response == false) {

                throw new Exception("Something wrong with parameters", 101);
            }

            $data = new SimpleXMLElement($curl_response);

            if($data->returncode->__toString() == 'FAILED') {

                throw new Exception($data->message->__toString(), 101);

            } else {

                // Proceed with after creating room

                $response_array = ['success' => true, 'data' => $data];

                return $response_array;

            }

        } catch(Exception $e) {

            $response_array = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return $response_array;

        }
    
    }

    public static function getRecordings($request, $meeting) {

        try {

            $params['internalMeetingID'] = $meeting->bbb_internal_meeting_id;

            $params['meetingID'] = $meeting->unique_id; 

            // $params['recordID'] = "record-".$meeting->unique_id; 

            $bbb_curl_url = self::generate_salt($params, $method = 'getRecordings');

            $response = self::bbb_meeting_curl_execute($bbb_curl_url);

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    public static function PublishRecordings($request, $meeting) {

        try {

            $params['internalMeetingID'] = $meeting->bbb_internal_meeting_id;

            // $params['meetingID'] = $meeting->unique_id; 

            // $params['recordID'] = "record-".$meeting->unique_id; 

            $params['publish'] = 'true';

            $bbb_curl_url = self::generate_salt($params, $method = 'publishRecordings');

            $response = self::bbb_meeting_curl_execute($bbb_curl_url);

            return response()->json($response, 200);

        } catch(Exception $e) {

            $response = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response, 200);

        }

    }

    public static function add_presentation($filename) {

        $result = "";
   
        $xml    = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><modules/>');
        $module = $xml->addChild('module');
        $module->addAttribute('name', 'presentation');

        $nameOrUrl = $filename;

        if (strpos($nameOrUrl, 'http') === 0) {
            $presentation = $module->addChild('document');
            $presentation->addAttribute('url', $nameOrUrl);
            if (is_string(true)) {
            }
        } else {
            $document = $module->addChild('document');
            $document->addAttribute('name', $nameOrUrl);
            $document[0] = true;
        }

        $result = $xml->asXML();

        return $result;

    }

    public static function sample() {

        // dd(self::add_presentation('http://bbschool-backend.codegama.net/Welcome-to-BBB-School.pdf'));

        // $bbb = new BigBlueButton();

        // $createMeetingParams = new CreateMeetingParameters("001", "Hello World");
        // $createMeetingParams->setAttendeePassword("ap");
        // $createMeetingParams->setModeratorPassword("mp");
        // $createMeetingParams->setDuration(60);
        // $createMeetingParams->setLogoutUrl("https://www.google.com/");

        // $createMeetingParams->addPresentation("http://bbschool-backend.codegama.net/Welcome-to-BBB-School.pdf");

        // $response = $bbb->createMeeting($createMeetingParams);


        // dd($response);

        // if ($response->getReturnCode() == 'FAILED') {
        //     return 'Can\'t create room! please contact our administrator.';
        // } else {
        //     // process after room created
        // }
    }
}