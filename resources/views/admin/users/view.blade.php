@extends('layouts.admin')

@section('page_header',tr('users'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.users.index')}}">{{tr('users')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('view_users')}}</li>
@endsection

@section('content')

<div class="card">

	<div class="card-header bg-info"> 

		<h4 class="m-b-0 text-white">{{tr('view_users')}}</h4>

	</div>

    <div class="card-body">

    	<div class="row">

    		<div class="col-6">

    			<img src="{{$user_details->picture}}" style="width: 100%; height: 50%">
    			<hr>

    				<div class="row">

                        <div class="col-6">

                            @if(Setting::get('is_demo_control_enabled') == YES)

                                <a href="javascript:;" class="btn btn-primary btn-block">{{tr('edit')}}</a>

                                <a href="javascript:;" class="btn btn-danger btn-block">{{tr('delete')}}</a>

                            @else

                                <a class="btn btn-primary btn-block" href="{{ route('admin.users.edit', ['user_id' => $user_details->id])}}">{{tr('edit')}}</a>

                                <a class="btn btn-danger btn-block" href="{{route('admin.users.delete', ['user_id' => $user_details->id])}}" onclick="return confirm(&quot;{{tr('user_delete_confirmation' , $user_details->name)}}&quot;);">{{tr('delete')}}</a>

                            @endif

                        </div>

                        <div class="col-6">

                        	@if($user_details->status == USER_APPROVED)

                                <a class="btn btn-danger btn-block" href="{{ route('admin.users.status', ['user_id' => $user_details->id]) }}" onclick="return confirm(&quot;{{$user_details->first_name}} - {{tr('user_decline_confirmation')}}&quot;);" >
                                    {{ tr('decline') }} 
                                </a>

                            @else
                                
                                <a class="btn btn-success btn-block" href="{{ route('admin.users.status', ['user_id' => $user_details->id]) }}">
                                    {{ tr('approve') }} 
                                </a>
                                   
                            @endif


                            @if($user_details->is_verified == USER_EMAIL_NOT_VERIFIED) 

                                <a class="btn btn-primary btn-block" href="{{ route('admin.users.verify', ['user_id' => $user_details->id]) }}"> {{ tr('verify') }} 
                                </a>   

                            @endif

                            <a class="btn btn-primary btn-block" href="{{ route('admin.meetings.index', ['user_id' => $user_details->id]) }}">
                                {{ tr('meetings') }} 
                            </a>

                            <a class="btn btn-primary btn-block" target="_blank" href="{{route('admin.users.subscriptions', ['user_id' => $user_details->id] )}}" class="btn btn-xs btn-success">{{tr('subscribe')}}</a>

                        </div> 

                    </div>

                <hr>

                <div class="row">

                    @if($user_details->description)

                        <h5 class="col-md-12">{{tr('description')}}</h5>
                     	<p class="col-md-12 text-muted text-word-wrap">{{$user_details->description}}</p>

                    @endif
                    
                </div>

    		</div>

            <div class="col-6">

                <!-- Card content -->
                <div class="card-body">

                    <div class="template-demo">

                        <table class="table mb-0">

                          <thead>
                           
                          </thead>

                          <tbody>

                            <tr>
                                <td class="pl-0"><b>{{ tr('name') }}</b></td>
                                <td class="pr-0 text-right"><div >{{$user_details->name}}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"><b>{{ tr('email') }}</b></td>
                                <td class="pr-0 text-right"><div >{{$user_details->email}}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"><b>{{ tr('mobile') }}</b></td>
                                <td class="pr-0 text-right"><div >{{$user_details->mobile ?:tr('not_available')}}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"><b>{{ tr('gender') }}</b></td>
                                <td class="pr-0 text-right"><div class="text-capitalize">{{ ucfirst($user_details->gender) }}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"><b>{{ tr('dob') }}</b></td>
                                <td class="pr-0 text-right"><div >{{common_date($user_details->dob,Auth::guard('admin')->user()->timezone,'d M Y')}}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"><b>{{ tr('device_type') }}</b></td>
                                <td class="pr-0 text-right"><div  class="text-capitalize">{{ ucfirst($user_details->device_type) }}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"><b>{{ tr('login_by') }}</b></td>
                                <td class="pr-0 text-right"><div  class="text-capitalize">{{ ucfirst($user_details->login_by) }}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"><b>{{ tr('register_type') }} </b></td>
                                <td class="pr-0 text-right"><div  class="text-capitalize">{{ ucfirst($user_details->register_type) }}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"><b>{{ tr('payment_mode') }} </b></td>
                                <td class="pr-0 text-right"><div >{{$user_details->payment_mode}}</div></td>
                            </tr>

                            <tr>
                                <td class="pl-0"> <b>{{ tr('timezone') }}</b></td>
                                <td class="pr-0 text-right"><div>{{$user_details->timezone}}</div></td>
                            </tr>

                            <tr>

                              <td class="pl-0"> <b>{{ tr('status') }}</b></td>

                              <td class="pr-0 text-right">

                                    @if($user_details->status == USER_PENDING)

                                        <span class="card-text badge badge-danger badge-md text-uppercase">{{tr('pending')}}</span>

                                    @elseif($user_details->status == USER_APPROVED)

                                        <span class="card-text  badge badge-success badge-md text-uppercase">{{tr('approved')}}</span>

                                    @else

                                        <span class="card-text label label-rouded label-menu label-danger">{{tr('declined')}}</span>

                                    @endif

                              </td>

                            </tr>

                            <tr> 

                                <td class="pl-0"><b>{{ tr('is_email_verified') }}</b></td>
                                
                                <td class="pr-0 text-right">

                                    @if($user_details->is_verified == USER_EMAIL_NOT_VERIFIED)

                                        <span class="card-text label label-rouded label-menu label-danger">{{ tr('no') }}</span>

                                    @else

                                        <span class="card-text badge badge-success badge-md text-uppercase">{{ tr('yes') }}</span>

                                    @endif
                                </td>

                            </tr>

                            <tr> 

                                <td class="pl-0"><b>{{ tr('push_notification') }}</b></td>
                                
                                <td class="pr-0 text-right">

                                    @if($user_details->push_notification_status)

                                        <span class="card-text label label-rouded label-menu label-danger">{{ tr('on') }}</span>

                                    @else

                                        <span class="card-text badge badge-success badge-md text-uppercase">{{ tr('off') }}</span>

                                    @endif
                                </td>

                            </tr>

                            <tr> 

                                <td class="pl-0"><b>{{ tr('email_notification') }}</b></td>
                                
                                <td class="pr-0 text-right">

                                    @if($user_details->email_notification_status)

                                        <span class="card-text label label-rouded label-menu label-danger">{{ tr('on') }}</span>

                                    @else

                                        <span class="card-text badge badge-success badge-md text-uppercase">{{ tr('off') }}</span>

                                    @endif
                                </td>

                            </tr>

                            <tr>
                                <td class="pl-0"> <b>{{ tr('created_at') }}</b></td>
                                <td class="pr-0 text-right"><div>{{ common_date($user_details->created_at, Auth::guard('admin')->user()->timezone,'d M Y H:i:s') }}</div></td>
                            </tr>

                            <tr>`
                                <td class="pl-0"> <b>{{ tr('updated_at') }}</b></td>
                                <td class="pr-0 text-right"><div>{{ common_date($user_details->updated_at,Auth::guard('admin')->user()->timezone, 'd M Y H:i:s') }}</div></td>
                            </tr>

                          </tbody>

                        </table>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>

@endsection

