@extends('layouts.admin')

@section('page_header',tr('dashboard'))

@section('breadcrumbs')

<li class="breadcrumb-item active"><a href="javascript:void(0)" aria-current="page"></a><span>{{tr('dashboard')}}</span></li>

@endsection

@section('content')

<div class="row">

    <div class="col-xl-4 col-sm-6">
        <div class="card card-default card-mini">
            <div class="card-header">
                <h2>{{$data->total_users}}</h2>
               
                <div class="sub-title">
                    <a href="{{route('admin.users.index')}}"><span class="mr-1">{{tr('total_users')}}</span></a>|
                   
                    <i class="mdi mdi-arrow-up-bold text-success"></i>
                </div>
            </div>
            
        </div>
    </div>    

    <div class="col-xl-4 col-sm-6">
        <div class="card card-default card-mini">
            <div class="card-header">
                <h2>{{$data->total_meetings}}</h2>
               
                <div class="sub-title">
                    <a href="{{route('admin.meetings.index')}}"><span class="mr-1">{{tr('total_meetings')}}</span></a> |
                   
                    <i class="mdi mdi-arrow-up-bold text-success"></i>
                </div>
            </div>
            
        </div>
    </div>

    <div class="col-xl-4 col-sm-6">
        <div class="card card-default card-mini">
            <div class="card-header">
                <h2>{{formatted_amount($data->subscription_amount)}}</h2>
               
                <div class="sub-title">
                <a href="{{route('admin.subscription.payments')}}"><span class="mr-1">{{tr('total_earnings')}}</span></a> |
                    
                    <i class="mdi mdi-arrow-up-bold text-success"></i>
                </div>
            </div>
            
        </div>
    </div>
   
    <div class="col-lg-6">
        <div class="card card-default">
            <div class="card-header">
                <h2>{{tr('recent_users')}}</h2>
            </div>
            @if(count($data->recent_users) > 0)
                <div class="card-body py-0" data-simplebar style="height: 392px;">
                    <table class="table table-borderless table-thead-border">
                        <thead>
                            <tr>
                                <th>{{tr('name')}}</th>
                                <th class="text-right">{{tr('time')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data->recent_users as $user_details)
        	                    <tr>
                                <td>
                                    <a href="{{ route('admin.users.view', ['user_id' => $user_details->id]) }}"><img
                                            src="{{$user_details->picture ?? asset('placeholder.jpg')}}" alt="user" width="40"
                                            class="img-circle" />&nbsp;&nbsp;{{$user_details->name}}
                                    </a>
                                </td>
        	                       
        	                        <td class="text-right">{{$user_details->created_at->diffForHumans()}}</td>
        	                    </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer bg-white d-flex py-4">
                    <a href="{{route('admin.users.index')}}" class="btn btn-primary">{{tr('view_all')}}</a>
                </div>

            @else
                <div class="card-body py-0">{{tr('no_user_found')}}</div>
            @endif
        </div>


    </div>



    <div class="col-lg-6">
        <div class="card card-default">
            <div class="card-header">
                <h2>{{tr('recent_meetings')}}</h2>
            </div>
            @if(count($data->recent_meetings) > 0)
                <div class="card-body py-0" data-simplebar style="height: 392px;">
                    <table class="table table-borderless table-thead-border">
                        <thead>
                            <tr>
                                <th>{{tr('meeting_name')}}</th>
                                <th class="text-right">{{tr('duration')}}</th>
                                <th class="text-right">{{tr('status')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data->recent_meetings as $meeting_details)
        	                    <tr>
        	                        <td class="text-primary"><a class="link" href="{{route('admin.meetings.view',['meeting_id' => $meeting_details->id])}}">{{$meeting_details->meeting_name}}</a></td>
        	                       
                                    <td class="text-right">{{$meeting_details->call_duration ?? calculate_call_duration($meeting_details->start_time,$meeting_details->end_time)}}</td>

                                    <td class="text-right">
                                    {{$meeting_details->meeting_status}}
						      	</td>
        	                    </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="card-footer bg-white d-flex py-4">
                    <a href="{{route('admin.meetings.index')}}" class="btn btn-primary">{{tr('view_all')}}</a>
                </div>

            @else
                <div class="card-body py-0">{{tr('no_user_found')}}</div>
            @endif
        </div>


    </div>

     
</div>

@endsection