<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />

<head>

    <meta charset="utf-8" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>{{Setting::get('site_name')}}</title>

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Karla:400,700|Roboto" rel="stylesheet">

    <link rel="shortcut icon" href="{{Setting::get('site_icon') ?? asset('favicon.png')}}">

    <link href="{{asset('admin-assets/plugins/material/css/materialdesignicons.min.css')}}" rel="stylesheet" />

    <link href="{{asset('admin-assets/plugins/simplebar/simplebar.css')}}" rel="stylesheet" />

    <!-- PLUGINS CSS STYLE -->
    <link href="{{asset('admin-assets/plugins/nprogress/nprogress.css')}}" rel="stylesheet" />

    <link href="{{asset('admin-assets/plugins/DataTables/DataTables-1.10.18/css/jquery.dataTables.min.css')}}" rel="stylesheet" />

    <link href="{{asset('admin-assets/plugins/jvectormap/jquery-jvectormap-2.0.3.css')}}" rel="stylesheet" />

    <link href="{{asset('admin-assets/plugins/daterangepicker/daterangepicker.css')}}" rel="stylesheet" />

    <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">

    <link href="{{asset('admin-assets/plugins/toaster/toastr.min.css')}}" rel="stylesheet" />
    
    <!-- Custom CSS -->
    <link id="main-css-href" rel="stylesheet" href="{{asset('admin-assets/css/style.css')}}" />

    <!-- CSS for Demo -->
    <link rel="stylesheet" href="{{asset('admin-assets/options/optionswitch.css')}}"/>

    <link href="{{asset('admin-assets/css/select2.min.css')}}" rel="stylesheet" />

    @yield('styles')
</head>

<body class="navbar-fixed sidebar-fixed" id="body">

@include('layouts.admin.scripts')

    <!-- <div id="toaster"></div> -->

    <div class="wrapper">

        @include('layouts.admin.sidebar')

        <div class="page-wrapper">

            @include('layouts.admin.header')

            <div class="content-wrapper">

                <div class="content">

                    <nav aria-label="breadcrumb">

                        <ol class="breadcrumb">

                            <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">{{tr('home')}}</a></li>
                            
                            @yield('breadcrumbs')
                            
                        </ol>
                    </nav>

                    <div class="row">

                        <div class="col-lg-12">

                            @include('notifications.notify')
                            
                            @yield('content')
                            
                        </div>

                    </div>

                </div>

            </div>
            
            @include('layouts.admin.footer')

        </div>

    </div>

    @include('layouts.admin._logout_model')

    @yield('scripts')

</body>

</html>