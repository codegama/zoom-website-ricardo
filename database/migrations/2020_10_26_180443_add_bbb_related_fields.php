<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBbbRelatedFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('meetings', function (Blueprint $table) {
            $table->string('bbb_internal_meeting_id')->default("");
            $table->string('bbb_voice_bridge')->default("");
            $table->string('bbb_dial_number')->default("");
            $table->string('bbb_record_id')->default("");
            $table->tinyInteger('is_recordings')->default(NO);
            $table->string('recording_url')->default("");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('meetings', function (Blueprint $table) {
            $table->dropColumn('bbb_internal_meeting_id');
            $table->dropColumn('bbb_voice_bridge');
            $table->dropColumn('bbb_dial_number');
            $table->dropColumn('bbb_record_id');
            $table->dropColumn('is_recordings');
            $table->dropColumn('recording_url');
        });
    }
}
