@extends('layouts.admin')

@section('page_header',tr('users'))

@section('breadcrumbs')

<li class="breadcrumb-item"><a href="{{route('admin.users.index')}}">{{tr('users')}}</a></li>

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('view_users')}}</li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('view_users')}}

            <button type="button" class="btn mb-2 btn-success" data-toggle="popover" data-content="{{tr('users_note')}}">{{tr('help')}}</button>
    
            <a class="btn btn-secondary pull-right" href="{{route('admin.users.create')}}">
                <i class="fa fa-plus"></i> {{tr('add_user')}}
            </a>

            <div class="dropdown btn btn-secondary pull-right admin_action">
                <button class="m-b-0 text-white" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{tr('admin_bulk_action')}}
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">
                    @if(Setting::get('is_demo_control_enabled') == NO)
                    <a class="action_list dropdown-item" id="bulk_delete" href="#">
                        {{tr('delete')}}
                    </a>    
                    @else
                    <a class="dropdown-item" href="javascript:;">{{tr('bulk_delete')}}</a>                           
                    @endif
                    <a class="action_list dropdown-item" id="bulk_approve" href="#">
                        {{tr('approve')}}
                    </a>   
                    <a class="action_list dropdown-item" id="bulk_decline" href="#">
                        {{tr('decline')}}
                    </a>

                </div>
            </div>
        </h4>

    </div>

    <div class="card-body">

        <div class="bulk_action">

                <form  action="{{route('admin.users.bulk_action')}}" id="admin_users" method="POST" role="search">

                    @csrf

                    <input type="hidden" name="action_name" id="action" value="">

                    <input type="hidden" name="selected_users" id="selected_ids" value="">

                    <input type="hidden" name="page_id" id="page_id" value="{{ (request()->page) ? request()->page : '1' }}">

                </form>
            </div>

        @include('admin.users._search')

        <div class="table-responsive">

            @if(count($users) > 0)

                <table id="actionDataTable" class="table data-table">

                    <thead>
                        <tr>
                            <th>
                                <input id="check_all" type="checkbox">
                            </th>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('name')}}</th>
                            <th>{{tr('email')}}</th>
                            <th>{{tr('mobile')}}</th>
                            <th>{{tr('meetings_clear')}}</th>
                            <th>{{tr('status')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>
                    </thead>

                    <tbody>

                        @foreach($users as $i => $user_details)
                            
                            <tr>
                                <td><input type="checkbox" name="row_check" class="faChkRnd" id="{{$user_details->id}}" value="{{$user_details->id}}"></td>
                                <td>{{$i+$users->firstItem()}}</td>

                                <td>
                                    <a href="{{ route('admin.users.view', ['user_id' => $user_details->id]) }}"><img
                                            src="{{$user_details->picture ?? asset('placeholder.jpg')}}" alt="user" width="40"
                                            class="img-circle" />&nbsp;&nbsp;{{$user_details->name}}
                                    </a>
                                </td>

                                <td>
                                    {{$user_details->email}}
                                </td>

                                <td>{{$user_details->mobile}}</td>

                                <td>
                                @if($user_details->is_cleared == tr('clear'))

                                <a href="{{route('admin.users.meetings_clear',['user_id'=>$user_details->id])}}" 
                                    onclick="return confirm('Are you sure about ongoing meetings clear?')"
                                >{{$user_details->is_cleared}}
                               </a>

                                @else
                                
                                    <a href="javascript:void(0)">{{$user_details->is_cleared}}</a>
                                @endif
                               </td>

                                <td>
                                    @if($user_details->status ==  YES)
                                        <span class="badge badge-success">{{tr('approved')}}
                                        </span>
                                    @else
                                        <span class="badge badge-danger">{{tr('declined')}}</span> 
                                    @endif
                                </td>

                                <td>
                                    
                                    <div class="dropdown">

                                        <button class="btn btn-outline-primary  dropdown-toggle btn-sm" type="button" id="dropdownMenuOutlineButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        {{tr('action')}}
                                        </button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuOutlineButton1">
                                          
                                            <a class="dropdown-item" href="{{ route('admin.users.view', ['user_id' => $user_details->id]) }}">
                                                {{tr('view')}}
                                            </a>
                                            
                                            @if(Setting::get('is_demo_control_enabled') == NO)

                                                <a class="dropdown-item" href="{{ route('admin.users.edit', ['user_id' => $user_details->id]) }}">
                                                    {{tr('edit')}}
                                                </a>
                                                
                                                <a class="dropdown-item" href="{{route('admin.users.delete', ['user_id' => $user_details->id,'page'=>request()->input('page')])}}" 
                                                onclick="return confirm(&quot;{{tr('user_delete_confirmation' , $user_details->name)}}&quot;);">
                                                    {{tr('delete')}}
                                                </a>
                                            @else

                                                <a class="dropdown-item" href="javascript:;">{{tr('edit')}}</a>
                                              
                                                <a class="dropdown-item" href="javascript:;">{{tr('delete')}}</a>                           
                                            @endif
                                            
                                            <div class="dropdown-divider"></div>

                                            @if($user_details->is_verified == USER_EMAIL_NOT_VERIFIED) 

                                                <a class="dropdown-item" href="{{ route('admin.users.verify', ['user_id' => $user_details->id]) }}"> {{ tr('verify') }} 
                                                </a>

                                            @endif 

                                            @if($user_details->status == USER_APPROVED)

                                                <a class="dropdown-item" href="{{ route('admin.users.status', ['user_id' => $user_details->id]) }}" onclick="return confirm(&quot;{{$user_details->first_name}} - {{tr('user_decline_confirmation')}}&quot;);" >
                                                    {{ tr('decline') }} 
                                                </a>

                                            @else
                                                
                                                <a class="dropdown-item" href="{{ route('admin.users.status', ['user_id' => $user_details->id]) }}">
                                                    {{ tr('approve') }} 
                                                </a>
                                                   
                                            @endif

                                            <a class="dropdown-item" href="{{ route('admin.meetings.index', ['user_id' => $user_details->id]) }}">
                                                {{ tr('meetings') }} 
                                            </a>

                                            <a class="dropdown-item" href="{{ route('admin.users.subscriptions' , ['user_id' => $user_details->id] ) }}">       
                                                {{ tr('subscription_plans') }}
                                            </a>


                                        </div>

                                    </div>

                                </td>
                             
                            </tr>
                        @endforeach
                       
                    </tbody>
                    
                </table>

                <div class="pull-right">{{ $users->appends(request()->input())->links() }}</div>

            @else

                <h3 class="no-result">{{ tr('no_user_found') }}</h3>
                
            @endif

        </div>
        
    </div>
    
</div>

@endsection

@section('scripts')
    
@if(Session::has('bulk_action'))
<script type="text/javascript">
    $(document).ready(function(){
        localStorage.clear();
    });
</script>
@endif

<script type="text/javascript">

    $(document).ready(function(){
        get_values();

        $('.action_list').click(function(){
            var selected_action = $(this).attr('id');
            if(selected_action != undefined){
                $('#action').val(selected_action);
                if($("#selected_ids").val() != ""){
                    if(selected_action == 'bulk_delete'){
                        var message = "<?php echo tr('admin_user_delete_confirmation') ?>";
                    }else if(selected_action == 'bulk_approve'){
                        var message = "<?php echo tr('admin_users_approve_confirmation') ?>";
                    }else if(selected_action == 'bulk_decline'){
                        var message = "<?php echo tr('admin_users_decline_confirmation') ?>";
                    }
                    var confirm_action = confirm(message);

                    if (confirm_action == true) {
                      $( "#admin_users" ).submit();
                    }
                    // 
                }else{
                    alert('Please select the check box');
                }
            }
        });
    // single check
    var page = $('#page_id').val();
    $(':checkbox[name=row_check]').on('change', function() {
        var checked_ids = $(':checkbox[name=row_check]:checked').map(function() {
            return this.id;
        })
        .get();

        localStorage.setItem("user_checked_items"+page, JSON.stringify(checked_ids));

        get_values();

    });
    // select all checkbox
    $("#check_all").on("click", function () {
        if ($("input:checkbox").prop("checked")) {
            $("input:checkbox[name='row_check']").prop("checked", true);
            var checked_ids = $(':checkbox[name=row_check]:checked').map(function() {
                return this.id;
            })
            .get();

            localStorage.setItem("user_checked_items"+page, JSON.stringify(checked_ids));
            get_values();
        } else {
            $("input:checkbox[name='row_check']").prop("checked", false);
            localStorage.removeItem("user_checked_items"+page);
            get_values();
        }

    });


    function get_values(){
        var pageKeys = Object.keys(localStorage).filter(key => key.indexOf('user_checked_items') === 0);
        var values = Array.prototype.concat.apply([], pageKeys.map(key => JSON.parse(localStorage[key])));

        if(values){
            $('#selected_ids').val(values);
        }

        for (var i=0; i<values.length; i++) {
            $('#' + values[i] ).prop("checked", true);
        }

}

});
</script>
@endsection