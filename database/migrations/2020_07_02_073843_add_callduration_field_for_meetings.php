<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCalldurationFieldForMeetings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        if (!Schema::hasColumn('meetings', 'call_duration'))
        {
            Schema::table('meetings', function (Blueprint $table) {
                $table->time('call_duration')->after('end_time')->nullable();
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        
        Schema::table('meetings', function (Blueprint $table) {
            $table->dropColumn('call_duration');
        });
    }
}
