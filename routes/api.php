<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any('meeting_records_save', 'Api\MeetingApiController@meeting_records_save')->name('meeting_records_save');

Route::any('meetings_end', 'Api\MeetingApiController@meetings_end')->name('meetings_end');

Route::any('meetings_users_logout', 'Api\MeetingApiController@meetings_users_logout')->name('meetings_users_logout');


Route::group(['prefix' => 'user' , 'middleware' => 'cors'], function() {

    Route::any('pages_list' , 'ApplicationController@static_pages_api');

    Route::get('get_settings_json', function () {

        if(\File::isDirectory(public_path(SETTINGS_JSON))){

        } else {

            \File::makeDirectory(public_path('default-json'), 0777, true, true);

            \App\Helpers\Helper::settings_generate_json();
        }

        $jsonString = file_get_contents(public_path(SETTINGS_JSON));

        $data = json_decode($jsonString, true);

        return $data;
    
    });

    Route::post('home' , 'Api\UserApiController@home');

	/***
	 *
	 * User Account releated routs
	 *
	 */

    Route::post('/register', 'Api\UserApiController@register');
    
    Route::post('/login','Api\UserApiController@login');

    Route::post('/forgot_password', 'Api\UserApiController@forgot_password');

    Route::post('/email_verification','Api\UserApiController@email_verification');
    
    Route::group(['middleware' => 'UserApiVal'] , function() {

        Route::post('/profile','Api\UserApiController@profile'); // 1

        Route::post('/update_profile', 'Api\UserApiController@update_profile'); // 2

        Route::post('/change_password', 'Api\UserApiController@change_password'); // 3

        Route::post('/delete_account', 'Api\UserApiController@delete_account'); // 4

        Route::post('/push_notification_update', 'Api\UserApiController@push_notification_status_change');  // 5

        Route::post('/email_notification_update', 'Api\UserApiController@email_notification_status_change'); // 6

        Route::post('/logout', 'Api\UserApiController@logout'); // 7

        // CARDS curd Operations

        Route::post('cards_add', 'Api\UserApiController@cards_add'); // 15

        Route::post('cards_list', 'Api\UserApiController@cards_list'); // 16

        Route::post('cards_delete', 'Api\UserApiController@cards_delete'); // 17

        Route::post('cards_default', 'Api\UserApiController@cards_default'); // 18

        Route::post('payment_mode_default', 'Api\UserApiController@payment_mode_default');
        //configurations

    });

    Route::post('/project/configurations', 'Api\UserApiController@configurations'); 

    Route::group(['middleware' => 'UserApiVal'] , function() {

        Route::post('subscriptions_index', 'Api\UserApiController@subscriptions_index');

        Route::post('subscriptions_view', 'Api\UserApiController@subscriptions_view');
        
        Route::post('subscriptions_payment_by_card', 'Api\UserApiController@subscriptions_payment_by_card');

        Route::post('subscriptions_payment_by_paypal', 'Api\UserApiController@subscriptions_payment_by_paypal');

        Route::post('subscriptions_history', 'Api\UserApiController@subscriptions_history');

    });

    Route::post('meetings_index', 'Api\MeetingApiController@meetings_index');

    Route::post('meetings_view', 'Api\MeetingApiController@meetings_view');

    Route::post('meetings_limit_check', 'Api\MeetingApiController@meetings_limit_check');

    Route::post('meetings_start', 'Api\MeetingApiController@meetings_start');
    
    Route::post('meetings_connection_update', 'Api\MeetingApiController@meetings_connection_update');

    Route::post('meetings_members', 'Api\MeetingApiController@meetings_members');

    Route::post('meetings_members_join', 'Api\MeetingApiController@meetings_members_join');

    Route::any('meetings_end', 'Api\MeetingApiController@meetings_end')->name('meetings_end');

    Route::post('meetings_status', 'Api\MeetingApiController@meetings_status');
    
    Route::any('meetings_users_logout', 'Api\MeetingApiController@meetings_users_logout')->name('meetings_users_logout');

});
