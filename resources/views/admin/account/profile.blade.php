
@extends('layouts.admin')

@section('page_header',tr('profile'))

@section('breadcrumbs')

<li class="breadcrumb-item active"><a href="javascript:void(0)">{{tr('profile')}}</a></li>

@endsection

@section('content')
<div class="row">
    <div class="col-lg-5 col-xlg-4 col-md-5">

        <div class="card">

            <div class="card-body">

                <center class="m-t-30"> <img src="{{$admin_details->picture  ?: asset('placeholder.png')}}" class="img-circle" width="150" />

                    <h4 class="card-title m-t-10">{{$admin_details->name}}</h4>

                    <h6 class="card-subtitle">{{$admin_details->timezone}}</h6>
                </center>

            </div>

            <hr>

            <div class="card-body"> 

                <small class="text-muted">{{tr('email_address')}}</small>
                <h6>{{$admin_details->email}}</h6> 

                <small class="text-muted p-t-30 db">{{tr('mobile')}}</small>
                <h6>{{$admin_details->mobile ?: "-"}}</h6> 

            </div>

        </div>

    </div>

    <div class="col-lg-7 col-xlg-9 col-md-7">

        <div class="card">
           
            <ul class="nav nav-tabs profile-tab" role="tablist">
                
                <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile_tab" role="tab">{{tr('profile')}}</a> </li>

                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings_tab" role="tab">{{tr('settings')}}</a> </li>

                <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#change_password_tab" role="tab">{{tr('change_password')}}</a> </li>

            </ul>
            
            <div class="tab-content">
               
                <div class="tab-pane active" id="profile_tab" role="tabpanel">

                    <div class="card-body">
                       
                        <h6>{{tr('about')}}</h6>

                        <hr>

                        <p class="m-t-30">{{$admin_details->about ?: "-"}}</p>

                    </div>

                </div>

                <div class="tab-pane" id="settings_tab" role="tabpanel">

                    <div class="card-body">

                        <form class="form-horizontal form-material" method="POST" action="{{route('admin.profile.save')}}" enctype="multipart/form-data" role="form">
                            @csrf
                            <div class="col-md-12">

                                <input type="hidden" name="admin_id" value="{{$admin_details->id}}"> 

                                <div class="form-group">
                                    <label>{{tr('full_name')}}<span class="admin-required">*</span></label>
                                    <input type="text" name="name" value="{{old('name') ?: $admin_details->name}}" placeholder="" class="form-control form-control-line" required>
                                </div>

                                <div class="form-group">
                                    <label for="email">{{tr('email')}}<span class="admin-required">*</span></label>
                                    <input type="email" placeholder="johnathan@admin.com" class="form-control form-control-line" name="email" value="{{old('email') ?: $admin_details->email}}" required> 
                                </div>

                                <div class="form-group">
                                    <label for="mobile">{{tr('mobile')}}<span class="admin-required">*</span></label>
                                    <input type="text" class="form-control form-control-line" name="mobile" value="{{old('mobile') ?: $admin_details->mobile}}" required>
                                </div>

                                <div class="form-group">
                                    <label class="about">{{tr('about')}}</label>
                                    <textarea rows="5" class="form-control form-control-line" name="about">{{old('about') ?: $admin_details->about}}</textarea>
                                </div>

                                <div class="form-group">
                                    <label for="picture">{{tr('picture')}}</label>
                                    <input type="file" accept="image/*"  class="form-control" name="picture" id="picture">     
                                </div>


                                <div class="form-group">
                                    <button type="submit" class="btn btn-success">Update Profile</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="tab-pane" id="change_password_tab" role="tabpanel">

                    <div class="card-body">

                        <h4>{{tr('change_password')}}</h4>
                        <hr>
                        <form class="form-horizontal form-material" method="POST" action="{{route('admin.change.password')}}">
                            @csrf
                            <div class="col-md-12"> 

                                <div class="form-group">
                                    <label for="old_password">{{tr('old_password')}}<span class="admin-required">*</span></label>
                                    <input id="password" type="password" name="old_password" value="{{old('old_password') ?: $admin_details->old_password}}"  class="form-control form-control-line" required>
                                </div>

                                <div class="form-group">
                                    <label for="password">{{tr('new_password')}}<span class="admin-required">*</span></label>
                                    <input id="new_password" type="password"  class="form-control form-control-line" name="password" value="{{old('password')}}" required> 
                                </div>

                                <div class="form-group">
                                    <label for="confirm_password">{{tr('confirm_password')}}<span class="admin-required">*</span></label>
                                    <input id="confirm_password" type="password" class="form-control form-control-line" name="confirm_password" value="{{old('confirm_password')}}" required>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-success" id="change_password_submit">{{tr('change_password')}}</button>
                                </div>
                                
                            </div>
                        </form>
                    </div>

                </div>

            </div>

        </div>

    </div>

</div>


<script type="text/javascript">
 
 $('body').on('click','#change_password_submit',function(){

  if($('#password').val()!=''  && $('#new_password').val()!='' && $('#confirm_password').val()!=''){
      
      alert("{{tr('password_change_confirmation')}}")
  }


 });

</script>


@endsection
