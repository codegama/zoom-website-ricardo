<?php

namespace App\Repositories;

use App\Helpers\Helper;

use App\Repositories\UserRepository as UserRepo;

use Log, Validator, Setting, Exception, DB;

use App\User;

use App\UserSubscription;

class PaymentRepository {

    /**
     * @method subscriptions_payment_save()
     *
     * @uses used to save user subscription payment details
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $subscription_details, object $request
     *
     * @return object $subscription_details
     */

    public static function subscriptions_payment_save($request, $subscription_details) {

        try {

            UserSubscription::where('user_id', $request->id)->where('is_current_subscription', YES)->update(['is_current_subscription' => NO]);

            $previous_payment = UserSubscription::where('user_id' , $request->id)
                                            ->where('status', PAID_STATUS)
                                            ->orderBy('created_at', 'desc')
                                            ->first();

            $user_subscription_details = new UserSubscription;

            $plan_type = "months"; // For future purpose, dont remove

            $user_subscription_details->expiry_date = date('Y-m-d H:i:s',strtotime("+{$subscription_details->plan} {$plan_type}"));

            if($previous_payment) {

                if (strtotime($previous_payment->expiry_date) >= strtotime(date('Y-m-d H:i:s'))) {
                    $user_subscription_details->expiry_date = date('Y-m-d H:i:s', strtotime("+{$subscription_details->plan} {$plan_type}", strtotime($previous_payment->expiry_date)));
                }
            }

            $user_subscription_details->subscription_id = $request->subscription_id;

            $user_subscription_details->user_id = $request->id;

            $user_subscription_details->payment_id = $request->payment_id ?? "NO-".rand();

            $user_subscription_details->status = PAID_STATUS;

            $user_subscription_details->amount = $request->paid_amount ?? 0.00;

            $user_subscription_details->payment_mode = $request->payment_mode ?? CARD;

            $user_subscription_details->is_current_subscription = YES;

            $user_subscription_details->no_of_hrs = $subscription_details->no_of_hrs ?? 1;

            $user_subscription_details->no_of_hrs_type = $subscription_details->no_of_hrs_type ?? HRS_TYPE_PER_DAY;

            $user_subscription_details->no_of_users = $subscription_details->no_of_users ?? 4;

            $user_subscription_details->save();

            $response_array = ['success' => true, 'message' => 'paid', 'data' => ['user_type' => SUBSCRIBED_USER, 'payment_id' => $request->payment_id]];

            return response()->json($response_array, 200);

        } catch(Exception $e) {

            $response_array = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response_array, 200);

        }
    
    }

    /**
     * @method subscriptions_payment_by_stripe()
     *
     * @uses pay for live videos using stripe
     *
     * @created vithya R
     * 
     * @updated vithya R
     *
     * @param object $subscription_details, object $request
     *
     * @return object $subscription_details
     */

    public static function subscriptions_payment_by_stripe($request, $subscription_details) {

        try {

            // Check stripe configuration
        
            $stripe_secret_key = Setting::get('stripe_secret_key');

            if(!$stripe_secret_key) {

                throw new Exception(api_error(107), 107);

            } 

            \Stripe\Stripe::setApiKey($stripe_secret_key);
           
            $currency_code = Setting::get('currency_code', 'USD') ?: "USD";

            $total = intval(round($request->user_pay_amount * 100));

            $charge_array = [
                                'amount' => $total,
                                'currency' => $currency_code,
                                'customer' => $request->customer_id,
                            ];


            $stripe_payment_response =  \Stripe\Charge::create($charge_array);

            $payment_data = [
                                'payment_id' => $stripe_payment_response->id ?? 'CARD-'.rand(),
                                'paid_amount' => $stripe_payment_response->amount/100 ?? $total,

                                'paid_status' => $stripe_payment_response->paid ?? true
                            ];

            $response_array = ['success' => true, 'message' => 'done', 'data' => $payment_data];

            return response()->json($response_array, 200);

        } catch(Exception $e) {

            $response_array = ['success' => false, 'error' => $e->getMessage(), 'error_code' => $e->getCode()];

            return response()->json($response_array, 200);

        }

    }

}