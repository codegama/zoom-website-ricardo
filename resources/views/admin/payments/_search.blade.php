<form class="search-button" action="{{route('admin.subscription.payments')}}" method="GET" role="search">

    <div class="row pt-2 pb-2">

        <div class="col-2">
              @if(Request::has('search_key'))
                <p class="text-muted">Search results for <b>{{Request::get('search_key')}}</b></p>
            @endif
        </div>

        <div class="col-2">

        <select class="form-control select2" name="status">

            <option class="select-color" value="">{{tr('select_status')}}</option>

            <option class="select-color" value="{{PAID}}" @if(Request::get('status') == PAID && Request::get('status')!='') selected @endif>{{tr('paid')}}</option>

            <option class="select-color" value="{{UNPAID}}" @if(Request::get('status') == UNPAID && Request::get('status')!='') selected @endif>{{tr('not_paid')}}</option>

        </select>

    </div>

        <div class="col-8">

                <!-- {{csrf_field()}} -->
                <div class="input-group">
                    <input type="text" class="form-control" name="search_key" value="{{Request::get('search_key') ?? ''}}"
                    placeholder="{{tr('subscription_payments_search_placeholder')}}"> 

                    <span class="input-group-btn">
                        &nbsp

                        <button type="submit" class="btn btn-primary">
                            {{tr('search')}}
                        </button>

                        <a class="btn btn-primary" href="{{route('admin.subscription.payments')}}">{{tr('clear')}}
                        </a>

                    </span>
                </div>


            </div>


        </div>

    </form>
