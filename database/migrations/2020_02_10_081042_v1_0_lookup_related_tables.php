<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class V10LookupRelatedTables extends Migration
{
    /**
     * Set up the options.
     *
     */
    public function __construct()
    {
        $this->table = config('setting.database.table');
        $this->key = config('setting.database.key');
        $this->value = config('setting.database.value');
    }

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable($this->table)) {

            Schema::create($this->table, function(Blueprint $table) {
                $table->increments('id');
                $table->string($this->key)->index();
                $table->text($this->value);
                $table->softDeletes();
                $table->timestamps();
            
            });
        }

        if(!Schema::hasTable('mobile_registers')) {

            Schema::create('mobile_registers', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('type');
                $table->integer('count');
                $table->tinyInteger('status')->default(APPROVED);
                $table->timestamps();
            });
        
        }

        if(!Schema::hasTable('page_counters')) {

            Schema::create('page_counters', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('page');
                $table->integer('count');
                $table->tinyInteger('status')->default(APPROVED);
                $table->timestamps();
            });

        }

        if(!Schema::hasTable('static_pages')) {

            Schema::create('static_pages', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('unique_id')->default(uniqid());
                $table->string('title')->unique();
                $table->text('description');
                $table->enum('type',['about','privacy','terms','refund','cancellation','faq','help','contact','others'])->default('others');
                $table->string('section_type')->nullable();
                $table->tinyInteger('status')->default(APPROVED);
                $table->softDeletes();
                $table->timestamps();
            });

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile_registers');
        Schema::dropIfExists('page_counters');
        Schema::dropIfExists('static_pages');
        Schema::dropIfExists($this->table);
    }
}
