@extends('layouts.admin')

@section('page_header',tr('subscriptions'))

@section('breadcrumbs')


<li class="breadcrumb-item"><a href="{{route('admin.users.index')}}">{{tr('users')}}</a></li>

<li class="breadcrumb-item"><a href="javascript:void(0)"></a>{{tr('view_users')}}</li>

<li class="breadcrumb-item active"><a href="{{route('admin.subscriptions.index')}}">{{tr('subscriptions')}}</a></li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{tr('user_subscriptions')}}</h4>

    </div>

    <div class="card-body">

        <div class="table-responsive">

            @if(count($user_subscriptions) > 0)

            <table id="dataTable" class="table data-table">

                <thead>
                    <tr>
                        <th>{{ tr('s_no') }}</th>
                        <th>{{ tr('subscriptions') }}</th>
                        <th>{{ tr('payment_id') }}</th>
                        <th>{{ tr('amount') }}</th>
                        <th>{{ tr('mode') }}</th>
                        <th>{{ tr('is_current') }}</th>
                        <th>{{ tr('expiry_date') }}</th>
                        <th>{{ tr('status') }}</th>
                        <th>{{ tr('created_at') }}</th>
                    </tr>
                </thead>

                <tbody>

                    @foreach($user_subscriptions as $i => $subscription_details)
                    <tr>
                        <td>{{ $i+$user_subscriptions->firstItem() }}</td>

                        <td>
                            <a href="{{ route('admin.subscriptions.view' , ['subscription_id' => $subscription_details->subscription_id] ) }}">{{ $subscription_details->title }}</a>
                        </td>

                        <td>{{ $subscription_details->payment_id }}</td>

                        <td>{{formatted_amount($subscription_details->amount)}}</td>

                        <td>{{ $subscription_details->payment_mode }}</td>

                        <td>
                            @if($subscription_details->is_current_subscription == YES)
                            <span class="badge badge-success">{{tr('yes')}}
                            </span>
                            @else
                            <span class="badge badge-danger">{{tr('no')}}</span>
                            @endif
                        </td>

                        <td>{{ $subscription_details->expiry_date }}</td>

                        <td>
                            @if($subscription_details->status == YES)
                            <span class="badge badge-success">{{tr('paid')}}
                            </span>
                            @else
                            <span class="badge badge-danger">{{tr('pending')}}</span>
                            @endif
                        </td>
                        <td>{{ common_date($subscription_details->created_at, Auth::guard('admin')->user()->timezone) }}</td>

                    </tr>
                    @endforeach

                </tbody>

            </table>

            <div class="pull-right">{{ $user_subscriptions->links() }}</div>

            @else

            <h3 class="no-result">{{ tr('no_subscriptions_found') }}</h3>

            @endif

        </div>

    </div>

</div>

<div class="row top_css">

    @if(count($subscriptions) > 0)

    @foreach($subscriptions as $s => $subscription_details)

    <div class="col-md-4">

        <div class="card subscription_scroll">

            <div class="card-body ">
                <h5 class="card-title">
                    <a href="{{ route('admin.subscriptions.view' , ['subscription_id' => $subscription_details->id] ) }}" target="_blank">{{ $subscription_details->title }}</a>
                </h5>
                <p class="card-text"><?php echo $subscription_details->description; ?></p>
            </div>
            <ul class="list-group list-group-flush">

                <li class="list-group-item ">{{formatted_amount($subscription_details->amount) }} / {{ formatted_plan($subscription_details->plan ?? 0) }} </li>

                <li class="list-group-item">{{ tr('no_of_users') }} : {{ $subscription_details->no_of_users }}</li>

            </ul>
            <div class="card-body">
                <a href="{{ route('admin.users.subscriptions.save', ['subscription_id' => $subscription_details->id, 'user_id' => $user_details->id]) }}" class="card-link ">{{ tr('choose') }}</a>
            </div>

        </div>

    </div>

    @endforeach

    @endif

</div>


@endsection