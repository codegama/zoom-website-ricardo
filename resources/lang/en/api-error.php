<?php

return [

	// Authentication
    1000 => 'Your account is waiting for admin approval',

    1001 => 'Please verify your email address!!',

    1002 => 'You are not a registered user!!',

    1003 => 'Token Expired',

    1004 => 'Invalid Token ',

    1005 => 'Authentication parameters are invalid.!',

    1006 => 'The record not exists.',

    1007 => 'Your account is waiting for admin approval.',

    1008 => 'Your account is waiting for admin approval.',

    1009 => 'Your account is waiting for admin approval',

    1010 => 'Your Email has been Verified already',

    101 => 'Invalid Input',

    102 => 'Sorry, the username or password you entered do not match. Please try again',

    103 => 'Oops! something went wrong. We couldn’t save your changes. Please try again.',

    104 => 'Invalid Email Address',

    105 => 'The mail send process is failed!!!',

    106 => 'The mail configuration failed!!!',

    107 => 'The payment is not configured properly!!!',

    108 => 'Sorry, the password is not matched.',

    109 => 'Update the payment mode in account and try again!!!',

    111 => 'Add card and try again.',

    113 => 'Payment Failed!!',

    114 => 'Failed to Add Card! Try after sometime!',

    115 => 'The forgot password only available for manual login.',

    116 => 'The email verification not yet done Please check you inbox.',

    117 => 'The requested email is disabled by admin.',

    118 => 'The change password only available for manual login.',

    119 => 'Account delete failed',

    120 => 'The card record is not found.',

    121 => 'The payment configuration Failed',

    122 => 'The card update failed.',

    123 => 'Registeration failed!!',

    124 => 'You do not have an account, please register and continue',

    127 => 'You have already logged into this account using another device, please logout and try again',

    128 => 'Details update failed!!',

    131 => 'You registered as viewer.',

    132 => 'Action requires the subscription. Please subscribe and continue',

    133 => 'Your account already upgraded as creator',

    134 => 'You already paid!!!',

    135 => 'The subscription record not found.',

    136 => 'Meeting is in-progress. Please try again!',

    137 => 'Meeting details not found',

    138 => 'The meeting allowed users limit exceeds.',

    139 => 'The allowed time limit exceeds.'

];
