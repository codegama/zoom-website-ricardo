@extends('layouts.admin')

@section('page_header',tr('subscription_payments'))

@section('breadcrumbs')

<li class="breadcrumb-item active"><a href="javascript:void(0)"></a>{{tr('subscription_payments')}}</li>

@endsection

@section('content')

<div class="card">

    <div class="card-header bg-info">

        <h4 class="m-b-0 text-white">{{$subscription_details}}   {{tr('subscription_payments')}} 
        </h4>

    </div>

	<div class="card-body">

        @include('admin.payments._search')

		<div class="table-responsive">

            @if(count($payments) > 0)

                <table id="dataTable" class="table data-table">

                    <thead>
                        <tr>
                            <th>{{tr('s_no')}}</th>
							<th>{{tr('username')}}</th>
							<th>{{tr('subscriptions')}}</th>
							<th>{{tr('mode')}}</th>
							<th>{{tr('payment_id')}}</th>
							<th>{{tr('plan')}} ({{Setting::get('currency')}})</th>
							<th>{{tr('paid')}} ({{Setting::get('currency')}})</th>
							<th>{{tr('expiry_date')}}</th>
							<th>{{tr('status')}}</th>
                        </tr>
                    </thead>

                    <tbody>

                    	@foreach($payments as $i => $payment_details)
                            <tr>
                                <td>{{$i+$payments->firstItem()}}</td>

                                <td>
                                    <a href="{{ route('admin.users.view', ['user_id' => $payment_details->user_id]) }}">{{($payment_details->user) ? $payment_details->user->name : tr('user_not_available')}}
                                    </a>
                                </td>

                                <td>
                                	@if($payment_details->subscription)
						      			<a href="{{route('admin.subscriptions.view' , ['subscription_id' => $payment_details->subscription->id] )}}" target="_blank">{{$payment_details->subscription ? $payment_details->subscription->title : ''}}</a>
						      		@else
						      			-
						      		@endif
                                </td>

                                <td class="text-capitalize">{{$payment_details->payment_mode ? $payment_details->payment_mode : 'free-plan'}}</td>

                                <td>
                                    <a href="{{route('admin.users.subscriptions.view' , ['user_subscription_id' => $payment_details->id] )}}" target="_blank">
                                    {{$payment_details->payment_id}}
                                   </a>
                                </td>

                                <td>{{formatted_amount($payment_details->subscription ? $payment_details->amount : "0.00")}}</td>

								<td>{{formatted_amount($payment_details->amount ? $payment_details->amount : "0.00")}}</td>
								      	
								<td>{{date('d M Y',strtotime($payment_details->expiry_date))}}</td>

                                <td>
						      		@if($payment_details->status)
									<span class="label label-success">{{tr('paid')}}</span>
									@else
									<span class="label label-danger">{{tr('not_paid')}}</span>
									@endif
						      	</td>
                             
                            </tr>
                        @endforeach
                       
                    </tbody>
                    
                </table>

                <div class="pull-right">{{ $payments->appends(request()->input())->links() }}</div>

            @else

                <h3 class="no-result">{{ tr('no_subscription_payments_found') }}</h3>
                
            @endif

        </div>
        
	</div>
	
</div>

@endsection