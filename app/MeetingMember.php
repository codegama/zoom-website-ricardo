<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MeetingMember extends Model
{
    protected $appends = ['meeting_member_id', 'meeting_member_unique_id'];

    protected $hidden = ['id', 'unique_id'];

    public function getMeetingMemberIdAttribute() {

        return $this->id;
    }

    public function getMeetingMemberUniqueIdAttribute() {

        return $this->unique_id;
    }

    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {

        return $query->leftJoin('meetings', 'meetings.id', '=', 'meeting_members.meeting_id')
        ->select(
            'meeting_members.*',
            'meetings.id as meeting_id',
            'meetings.unique_id as meeting_unique_id',
            'meetings.meeting_name',
            'meetings.username'
            );    
    }

    /**
     * Get the Meeting that owns the MeetingMember.
     */
    public function meeting() {

        return $this->belongsTo(Meeting::class, 'meeting_id');
    }

}
